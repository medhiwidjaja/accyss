defmodule AccyssWeb.PageController do
  use AccyssWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
