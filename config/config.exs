# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :accyss,
  ecto_repos: [Accyss.Repo]

# Configures the endpoint
config :accyss, AccyssWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FQWwtKJhgfVCYakjUN2BL5kyHtDZdkVdQ7SdQroIR/c3T6l5/h3atx6IUwH61gy6",
  render_errors: [view: AccyssWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Accyss.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
